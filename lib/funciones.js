console.log("Hola");

//Se declara la variable que tendrá el código transformado y el arreglo que va a guardar las líneas transformadas
let code=''
let convertir=[]


//método run donde hace toda la magia
function run(){

    //Guarda el contenido del textArea en programa
    let programa = document.getElementById("comment").value+"\n";
    //Lo enseña en la consola
    console.log(programa);
    //separa el programa por líneas y las va guardando en lineas haciendo uso de split
    let lineas = programa.split(":\n");
    //enseña las líneas en consola
    console.log(lineas);

    
    //For en donde se va a ir validando la sintaxys del programa, haciendo uso de match y de replace
    //se irán guardando las líneas transformadas en la posición en la que esté al arreglo convertir
    //estas se irán modificando según sea el caso. 

    for(var i=0; i<=lineas.length-1; i++){

        if (lineas[i].match('maye')!=null){
            //  Números enteros
            convertir.push(lineas[i].replace(/maye/,'let'))
            }
        else if(lineas[i].match('fex')!=null){
            //  Cadenas
            convertir.push(lineas[i].replace(/fex/,'let'))
        }
        else if(lineas[i].match('<<jos')!=null){
            //  Leer datos
            convertir.push(lineas[i].replace(/<<jos/,'=prompt()'))
        }
        else if(lineas[i].match('manux>>')!=null){
            //  Imprimir datos
            convertir.push(lineas[i].replace(/manux>>/,'alert('))
            convertir[i]+=")"
        }
        else if(lineas[i].match('sergio')!=null){
            //  For
            convertir.push(lineas[i].replace(/sergio/,'for'))
        }
        else{
            convertir.push(lineas[i])
        }

    }


    //Se enseña la lista convertir en la consola
    console.log(convertir)


    //for en donde se almacena las líneas convertidas a una variable 'code' para que esta pueda obtener todo el código 
    //transformado a un lenguaje que pueda ser compilado
    for(var j=0; j<=convertir.length-1; j++){
        code+=convertir[j]+"\n"
    }

    //Se enseña el resultado final en consola
    console.log(code)
    


    //Finalmente se implementa el código dentro de una función haciendo uso de document.write
    //para que esto se quede dentro de un script el cual se ejecutará una vez ejecutada la instrucción

    document.write("<script> function aver(){"+code+"} try{aver(); location.reload(); }catch(err){document.write(err.message);}</script>")

}



    //document.getElementById('compilar').innerHTML="<script> function aver(){"+code+"}</script>";
    //var jeje="<script> function aver(){"+code+"}</script>"
    //document.write(jeje)